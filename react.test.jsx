import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import { afterEach, describe, expect, it } from "vitest";
import { Calculator, operations, numbers, equalSign } from "./src/calculator";

describe('Calculator', () =>{
    afterEach(cleanup)

    it('should render', function(){
        render(<Calculator/>)
    })

    it('should render title correctly', function(){
        render(<Calculator />)
        screen.getByText('Calculator')
    })

    it('should render number', function(){
        render(<Calculator />)

        numbers.forEach(function(number){
            screen.getByText(number)
        })

    })

    it('should render 4 rows', function(){
        render(<Calculator />)

        const rows = screen.getAllByRole('row')
        expect(rows.length).toBe(4)
    })

    it('should render operations', function(){
        render(<Calculator />)

        operations.forEach(function(op){
            screen.getByText(op)
        })
    })

    it('should render equal sign', function(){
        render(<Calculator />)
        screen.getByText(equalSign)
    })

    it('should render input', function(){
        render(<Calculator />)
        screen.getByRole('textbox')
    })

    it('should let user input after clicking number', function(){
        render(<Calculator />)
        const one = screen.getByText('1')

        fireEvent.click(one)
        const input = screen.getByRole('textbox')
        expect(input.value).toBe('1')
    })

    it('should let user input after clicking several number', function(){
        render(<Calculator />)
        const one = screen.getByText('1') 
        fireEvent.click(one)

        const two = screen.getByText('2') 
        fireEvent.click(two)

        const three = screen.getByText('3') 
        fireEvent.click(three)

        const input = screen.getByRole('textbox')
        expect(input.value).toBe('123')
    })

    it('should let user input after clicking numbers and operations', function(){
        render(<Calculator />)
        const one = screen.getByText('1') 
        fireEvent.click(one)

        const op = screen.getByText('+') 
        fireEvent.click(op)
        fireEvent.click(one)

        const input = screen.getByRole('textbox')
        expect(input.value).toBe('1+1')
    })

    it('should calculate based on user input and calculate', function(){
        render(<Calculator />)
        const one = screen.getByText('1') 
        fireEvent.click(one)

        const op = screen.getByText('+') 
        fireEvent.click(op)
        fireEvent.click(one)

        const equal = screen.getByText(equalSign)
        fireEvent.click(equal)

        const input = screen.getByRole('textbox')
        expect(input.value).toBe('2')
    })

})